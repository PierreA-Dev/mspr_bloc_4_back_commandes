const express = require("express");
const { collection, addDoc, getDocs, getDoc, doc, updateDoc, deleteDoc, setDoc } = require("firebase/firestore");
const authenticateToken = require('../middleware/authenticateToken');
const { PubSub } = require('@google-cloud/pubsub');
require('dotenv').config();

// Initialiser Pub/Sub client
const pubSubClient = new PubSub();
const TOPIC_NAME = "channel_commandes";

const createPostRoutes = (db) => {
  const router = express.Router();

  // Fonction pour publier un message dans Pub/Sub
  async function publishMessage(message) {
    const dataBuffer = Buffer.from(JSON.stringify(message));
    try {
      await pubSubClient.topic(TOPIC_NAME).publish(dataBuffer);
      console.log(`Message publié dans Pub/Sub : ${JSON.stringify(message)}`);
    } catch (error) {
      console.error('Erreur lors de la publication du message dans Pub/Sub:', error.message);
    }
  }

  // Route pour créer une commande
  router.post("/commandes", authenticateToken, async (req, res) => {
    try {
      const { email, numcommandes, refproduits, libelle, quantite, conditionnement, etat } = req.body;

      // Vérification des champs obligatoires
      if (!email || !numcommandes || !refproduits || !libelle || !quantite || !conditionnement || !etat) {
        return res.status(400).json({ error: "Tous les champs obligatoires ne sont pas remplis." });
      }

      const commandesRef = doc(db, "commandes", email);
      const numCommandesRef = doc(commandesRef, "numcommandes", numcommandes);
      const refProduitsRef = doc(numCommandesRef, "refproduits", refproduits);

      await setDoc(numCommandesRef, { numcommandes });
      await setDoc(refProduitsRef, { libelle, quantite, conditionnement, etat });

      // Publier un message dans Pub/Sub lorsque la commande est créée
      const message = {
        action: 'CREATE',
        email,
        numcommandes,
        refproduits,
        libelle,
        quantite,
        conditionnement,
        etat
      };
      await publishMessage(message); 

      res.json({ message: "Commande créée avec succès!" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  // Route pour récupérer toutes les commandes
  router.get('/commandes', authenticateToken, async (req, res) => {
    try {
      const commandesRef = collection(db, 'commandes');
      const querySnapshot = await getDocs(commandesRef);
      const documents = [];

      // Récupération des données depuis Firestore
      for (const userDoc of querySnapshot.docs) {
        const numCommandesSnapshot = await getDocs(collection(userDoc.ref, 'numcommandes'));
        for (const numCommande of numCommandesSnapshot.docs) {
          const refProduitsSnapshot = await getDocs(collection(numCommande.ref, 'refproduits'));
          for (const refProduit of refProduitsSnapshot.docs) {
            documents.push({
              email: userDoc.id,
              numcommandes: numCommande.id,
              refproduits: refProduit.id,
              ...refProduit.data(),
            });
          }
        }
      }
      res.status(200).json(documents);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  // Route pour lire les commandes par email
  router.get("/commandes/:email", authenticateToken, async (req, res) => {
    try {
      const { email } = req.params;

      const commandesRef = doc(db, "commandes", email);
      const numCommandesSnapshot = await getDocs(collection(commandesRef, "numcommandes"));
      const documents = [];
      for (const numCommande of numCommandesSnapshot.docs) {
        const refProduitsSnapshot = await getDocs(collection(numCommande.ref, "refproduits"));
        for (const refProduit of refProduitsSnapshot.docs) {
          documents.push({
            email: email,
            numcommandes: numCommande.id,
            refproduits: refProduit.id,
            ...refProduit.data()
          });
        }
      }

      if (documents.length > 0) {
        res.json(documents);
      } else {
        res.status(404).json({ message: "Aucune commande trouvée!" });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  // Route pour mettre à jour une commande
  router.put("/commandes/:email/:numcommandes/:refproduits", authenticateToken, async (req, res) => {
    try {
      const { email, numcommandes, refproduits } = req.params;
      const { libelle, quantite, conditionnement, etat } = req.body;

      // Vérification des champs obligatoires
      if (!libelle || !quantite || !conditionnement || !etat) {
        return res.status(400).json({ error: "Tous les champs obligatoires ne sont pas remplis." });
      }

      const commandesRef = doc(db, "commandes", email);
      const numCommandesRef = doc(commandesRef, "numcommandes", numcommandes);
      const refProduitsRef = doc(numCommandesRef, "refproduits", refproduits);

      await updateDoc(refProduitsRef, { libelle, quantite, conditionnement, etat });

      // Publier un message dans Pub/Sub lorsque la commande est mise à jour
      const message = {
        action: 'UPDATE',
        email,
        numcommandes,
        refproduits,
        libelle,
        quantite,
        conditionnement,
        etat
      };
      await publishMessage(message);

      res.json({ message: "Commande mise à jour avec succès!" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  // Route pour supprimer une commande
  router.delete("/commandes/:email/:numcommandes/:refproduits", authenticateToken, async (req, res) => {
    try {
      const { email, numcommandes, refproduits } = req.params;

      const commandesRef = doc(db, "commandes", email);
      const numCommandesRef = doc(commandesRef, "numcommandes", numcommandes);
      const refProduitsRef = doc(numCommandesRef, "refproduits", refproduits);

      await deleteDoc(refProduitsRef);

      // Publier un message dans Pub/Sub lorsque la commande est supprimée
      const message = {
        action: 'DELETE',
        email,
        numcommandes,
        refproduits
      };
      await publishMessage(message);

      res.json({ message: "Commande supprimée avec succès!" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  });

  return router;
};

module.exports = createPostRoutes;