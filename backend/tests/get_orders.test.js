const request = require("supertest");
const express = require("express");
const { collection, getDocs } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes");


// Mock de Firestore
jest.mock("firebase/firestore", () => ({
  collection: jest.fn(),
  getDocs: jest.fn(() => ({
    docs: [
      {
        id: "commande1",
        data: () => ({
          email: "test@example.com",
          numcommandes: "12345",
          refproduits: "PROD001",
          libelle: "Produit Test",
          quantite: 10,
          conditionnement: "Boîte",
          etat: "En cours",
        }),
      },
    ],
  })),
}));


// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: "testUserId" };
    next();
  };
});

// Configuration de l'application avec les routes
const app = express();
app.use(express.json());
app.use("/", createPostRoutes({}));

describe("Tests API pour récupérer toutes les commandes", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("devrait récupérer toutes les commandes depuis Firestore", async () => {
    // Mock des données Firestore
    const mockDocs = [
      {
        id: "commande1",
        data: () => ({
          email: "test@example.com",
          numcommandes: "12345",
          refproduits: "PROD001",
          libelle: "Produit Test",
          quantite: 10,
          conditionnement: "Boîte",
          etat: "En cours",
        }),
      },
    ];
    const mockQuerySnapshot = { docs: mockDocs };

    // Mock de Firestore
    getDocs.mockResolvedValueOnce(mockQuerySnapshot);

    const response = await request(app)
      .get("/commandes")
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual([
      {
        email: "test@example.com",
        numcommandes: "12345",
        refproduits: "PROD001",
        libelle: "Produit Test",
        quantite: 10,
        conditionnement: "Boîte",
        etat: "En cours",
      },
    ]);
  });

  it("devrait retourner une erreur si Firestore échoue", async () => {
    // Simuler une erreur de Firestore
    getDocs.mockRejectedValueOnce(new Error("Erreur Firestore"));

    const response = await request(app)
      .get("/commandes")
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ error: "Erreur Firestore" });
  });
});