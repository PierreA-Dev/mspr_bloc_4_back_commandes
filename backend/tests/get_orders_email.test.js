const request = require("supertest");
const express = require("express");
const { getDocs, collection, doc } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes");


// Mock complet de Firestore
jest.mock("firebase/firestore", () => ({
  getDocs: jest.fn(),
  collection: jest.fn(),
  doc: jest.fn(() => ({})),
}));

// Mock du middleware d'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: "testUserId" };
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({}));

it("devrait retourner une erreur si aucune commande n'est trouvée pour l'utilisateur", async () => {
    const email = "test@example.com";

    // Simuler un cas où Firestore ne retourne pas de commandes
    getDocs.mockResolvedValueOnce({
      docs: [],
    });
});