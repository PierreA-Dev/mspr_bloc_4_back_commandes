const request = require("supertest");
const express = require("express");
const { setDoc, doc } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes");

// Mock de Firestore
jest.mock("firebase/firestore", () => ({
  setDoc: jest.fn(),
  doc: jest.fn(() => ({})),
}));

// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: "testUserId" };
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({}));

describe("Tests API pour créer une commande", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("devrait créer une commande avec succès", async () => {
    const commande = {
      email: "test@example.com",
      numcommandes: "12345",
      refproduits: "PROD001",
      libelle: "Produit Test",
      quantite: 10,
      conditionnement: "Boîte",
      etat: "En cours",
    };

    // Mock du comportement Firestore
    setDoc.mockResolvedValueOnce(); // Simuler que l'appel à `setDoc` réussit

    const response = await request(app)
      .post("/commandes")
      .send(commande)
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ message: "Commande créée avec succès!" });

    // Vérifie que `setDoc` a été appelé deux fois (pour numcommandes et refproduits)
    expect(setDoc).toHaveBeenCalledTimes(2);
  });

  it("devrait retourner une erreur si des champs obligatoires sont manquants", async () => {
    const commande = {
      email: "test@example.com", // Données incomplètes
    };

    const response = await request(app)
      .post("/commandes")
      .send(commande)
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(400);
    expect(response.body).toEqual({
      error: "Tous les champs obligatoires ne sont pas remplis.",
    });

    // Vérifie que `setDoc` n'est pas appelé
    expect(setDoc).not.toHaveBeenCalled();
  });

  it("devrait retourner une erreur si Firestore échoue", async () => {
    const commande = {
      email: "test@example.com",
      numcommandes: "12345",
      refproduits: "PROD001",
      libelle: "Produit Test",
      quantite: 10,
      conditionnement: "Boîte",
      etat: "En cours",
    };

    // Simuler une erreur lors de l'appel à `setDoc`
    setDoc.mockRejectedValueOnce(new Error("Erreur Firestore"));

    const response = await request(app)
      .post("/commandes")
      .send(commande)
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ error: "Erreur Firestore" });

    // Vérifie que `setDoc` a bien été appelé avant l'erreur
    expect(setDoc).toHaveBeenCalledTimes(1);
  });
});