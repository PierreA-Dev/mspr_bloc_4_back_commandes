const request = require("supertest");
const express = require("express");
const { deleteDoc, doc } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes");


// Mock de Firestore
jest.mock("firebase/firestore", () => ({
  deleteDoc: jest.fn(),
  doc: jest.fn(() => ({})),
}));

// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: "testUserId" };
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({}));

describe("Tests API pour supprimer une commande", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("devrait supprimer une commande avec succès", async () => {
    // Mock de la suppression dans Firestore
    deleteDoc.mockResolvedValueOnce(); // Simuler que l'appel à `deleteDoc` réussit

    const response = await request(app)
      .delete("/commandes/test@example.com/12345/PROD001")
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ message: "Commande supprimée avec succès!" });

    // Vérifie que `deleteDoc` a été appelé une fois
    expect(deleteDoc).toHaveBeenCalledTimes(1);
  });

  it("devrait retourner une erreur si la commande n'existe pas", async () => {
    // Simuler une erreur lors de la suppression
    deleteDoc.mockRejectedValueOnce(new Error("Aucune commande trouvée"));

    const response = await request(app)
      .delete("/commandes/test@example.com/12345/PROD001")
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ error: "Aucune commande trouvée" });

    // Vérifie que `deleteDoc` a bien été appelé
    expect(deleteDoc).toHaveBeenCalledTimes(1);
  });

  it("devrait retourner une erreur si Firestore échoue", async () => {
    // Simuler une erreur générale lors de la suppression
    deleteDoc.mockRejectedValueOnce(new Error("Erreur Firestore"));

    const response = await request(app)
      .delete("/commandes/test@example.com/12345/PROD001")
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ error: "Erreur Firestore" });

    // Vérifie que `deleteDoc` a bien été appelé
    expect(deleteDoc).toHaveBeenCalledTimes(1);
  });
});