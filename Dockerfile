# Utiliser l'image officielle de Node.js
FROM node:20.18-alpine

# Créer et définir le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copier les fichiers de dépendances dans le conteneur
COPY package*.json ./

# Installer les dépendances de l'application
RUN npm install

# Copier le code source de l'application dans le conteneur
COPY . .

# Exposer le port sur lequel l'application va tourner
EXPOSE 4000

# Commande pour démarrer l'application
CMD ["node", "backend/server.js"]