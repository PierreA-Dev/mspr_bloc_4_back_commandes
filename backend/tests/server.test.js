const request = require("supertest");
const express = require("express");
const cors = require("cors");
const postRoutes = require("../routes/post.routes");
const app = require('../server');  // Importer l'application sans démarrer le serveur


jest.mock("firebase/app", () => ({
  initializeApp: jest.fn(),
}));

jest.mock("firebase/firestore", () => ({
  getFirestore: jest.fn(),
}));

jest.mock("../routes/post.routes", () => jest.fn(() => (req, res) => res.send("Post routes working")));

describe("server.js", () => {
  let app;

  beforeEach(() => {
    app = express();

    // Configuration CORS
    app.use(cors({
      origin: 'https://mspr-bloc-4-front-405778108809.europe-west9.run.app'
    }));

    // Middleware pour traiter les données de la requête
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    // Simulation des routes
    const mockDb = {}; // Mock d'une instance de Firestore
    app.use("/", postRoutes(mockDb));
  });

  test("CORS configuration should allow requests from the correct origin", async () => {
    const response = await request(app)
      .get("/")
      .set("Origin", "https://mspr-bloc-4-front-405778108809.europe-west9.run.app");

    expect(response.status).toBe(200);
    expect(response.text).toBe("Post routes working");
    expect(response.headers["access-control-allow-origin"]).toBe("https://mspr-bloc-4-front-405778108809.europe-west9.run.app");
  });

  test("CORS configuration should block requests from an unauthorized origin", async () => {
    const response = await request(app)
      .get("/")
      .set("Origin", "https://unauthorized-domain.com");

    expect(response.status).toBe(200);
  });

  test("Should return 200 and 'Post routes working' for the root route", async () => {
    const response = await request(app).get("/");

    expect(response.status).toBe(200);
    expect(response.text).toBe("Post routes working");
  });

  test("Should handle POST requests with JSON payload", async () => {
    const payload = { key: "value" };

    const response = await request(app)
      .post("/")
      .send(payload)
      .set("Content-Type", "application/json");

    expect(response.status).toBe(200);
    expect(response.text).toBe("Post routes working");
  });
});
