const { initializeApp } = require("firebase/app");
const { getFirestore } = require("firebase/firestore");
const express = require("express");
const postRoutes = require("./routes/post.routes");
const cors = require('cors');

const app = express();  // Création de l'application Express

// Désactiver l'en-tête X-Powered-By pour éviter de divulguer la version d'Express
app.disable('x-powered-by');


// Configuration CORS
app.use(cors({
  origin: 'https://mspr-bloc-4-front-405778108809.europe-west9.run.app' // Adresse du serveur frontend
}));

// Configuration Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBzS3pmZs0WMzNA40LBHuUG8GZRx0Co8ig",
  authDomain: "mspr-bloc-4-5135a.firebaseapp.com",
  projectId: "mspr-bloc-4-5135a",
  storageBucket: "mspr-bloc-4-5135a.appspot.com",
  messagingSenderId: "405778108809",
  appId: "1:405778108809:web:2f6542855e3f78b6d51158",
  measurementId: "G-G1S80GHD76"
};

// Initialiser Firebase
const appFirebase = initializeApp(firebaseConfig);

// Initialiser Cloud Firestore
const db = getFirestore(appFirebase);

// Middleware pour traiter les données de la requête
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Passer l'instance de Firestore aux routes
app.use("/", postRoutes(db));

// Exporter l'application pour les tests
module.exports = app;

// Démarrage du serveur uniquement si le fichier est exécuté directement
if (require.main === module) {
  const PORT = process.env.PORT || 4000;
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
}