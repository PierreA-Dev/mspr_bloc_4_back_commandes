const request = require("supertest");
const express = require("express");
const { updateDoc, doc } = require("firebase/firestore");
const createPostRoutes = require("../routes/post.routes");


// Mock de Firestore
jest.mock("firebase/firestore", () => ({
  updateDoc: jest.fn(),
  doc: jest.fn(() => ({})),
}));

// Mock de l'authentification
jest.mock("../middleware/authenticateToken", () => {
  return (req, res, next) => {
    req.user = { id: "testUserId" };
    next();
  };
});

const app = express();
app.use(express.json());
app.use("/", createPostRoutes({}));

describe("Tests API pour mettre à jour une commande", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("devrait mettre à jour une commande avec succès", async () => {
    const commande = {
      libelle: "Produit Modifié",
      quantite: 20,
      conditionnement: "Boîte modifiée",
      etat: "Expédiée",
    };

    // Mock de la mise à jour de Firestore
    updateDoc.mockResolvedValueOnce(); // Simuler que l'appel à `updateDoc` réussit

    const response = await request(app)
      .put("/commandes/test@example.com/12345/PROD001")
      .send(commande)
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(200);
    expect(response.body).toEqual({ message: "Commande mise à jour avec succès!" });

    // Vérifie que `updateDoc` a été appelé une fois
    expect(updateDoc).toHaveBeenCalledTimes(1);
  });

  it("devrait retourner une erreur si des champs obligatoires sont manquants", async () => {
    const commande = {
      libelle: "Produit Modifié", // Données incomplètes (par exemple manque quantite, conditionnement, etat)
    };

    const response = await request(app)
      .put("/commandes/test@example.com/12345/PROD001")
      .send(commande)
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(400);
    expect(response.body).toEqual({
      error: "Tous les champs obligatoires ne sont pas remplis.",
    });

    // Vérifie que `updateDoc` n'est pas appelé
    expect(updateDoc).not.toHaveBeenCalled();
  });

  it("devrait retourner une erreur si Firestore échoue", async () => {
    const commande = {
      libelle: "Produit Modifié",
      quantite: 20,
      conditionnement: "Boîte modifiée",
      etat: "Expédiée",
    };

    // Simuler une erreur lors de l'appel à `updateDoc`
    updateDoc.mockRejectedValueOnce(new Error("Erreur Firestore"));

    const response = await request(app)
      .put("/commandes/test@example.com/12345/PROD001")
      .send(commande)
      .set("Authorization", "Bearer fakeToken");

    expect(response.statusCode).toBe(500);
    expect(response.body).toEqual({ error: "Erreur Firestore" });

    // Vérifie que `updateDoc` a bien été appelé avant l'erreur
    expect(updateDoc).toHaveBeenCalledTimes(1);
  });
});